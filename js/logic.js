/* Define global vars */
var memoryGame = document.getElementById('memoryGame');
var elements = [];
var countClick = 0;
var choose1 = '';
var lastId = '';
var lastElement = null;
var currentElement = null;
var parent = 0;
/**
 * Method to initialize the game
 * @return {void}
 */
function initGame(){
    elements = generateMatrix();
    buildBoard();
    addEventClickInElements();
}
/**
 * Method to build game board
 * @return {void}
 */
function buildBoard(){
    var nodeBase = document.createElement('div');
    nodeBase.className = 'item';
// nodeBase.appendChild(document.createElement('h1'));
    var posItem = 0;
    elements.forEach(function (row){
        row.forEach(function (col){
            let clone = nodeBase.cloneNode(true);
            clone.setAttribute('id', 'item' + posItem);
// clone.getElementsByTagName('h1')[0].innerHTML = col;
//clone.innerHTML = col;
            console.log(col);
            clone.dataset.posRow = elements.indexOf(row);
            clone.dataset.posCol = row.indexOf(col);
            clone.dataset.value = col;
            memoryGame.appendChild(clone);
            posItem++;
        });
    });
}
/**
 * Method to generate matrix of the game board
 * @return {array}
 */
function generateMatrix(){
  let buildArray = [
    ['0', '0', '0'],
    ['0', '0', '0'],
    ['0', '0', '0'],
    ['0', '0', '0'],
  ];
  let alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M',
                  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  let partners = [];
  let raNum;
  for(var i = 0, quantity = 25; i < 6; i++, quantity--){
    raNum = Math.floor(quantity*(Math.random()));
    partners.push(alphabet[raNum]);
    alphabet.splice(raNum,1);
  }
  partners = partners.concat(partners);
  console.log("The Elements are: "+partners);
  quantity = 11;
  let large = buildArray.length;
  let large2;
  let arrayAux;
  for(var i = 0; i < large; i++){
    large2 = buildArray[i].length;
    arrayAux = buildArray[i]
    for(var j = 0; j < large2; j++){
      raNum = Math.floor(quantity*(Math.random()));
      arrayAux[j]= partners[raNum];
      console.log("The partner element is: "+partners[raNum]);
      console.log("The array element is: "+arrayAux[j]);
      partners.splice(raNum,1);
      quantity--;
    }
  }
  /*
  buildArray.forEach(function (row){
    row.forEach(function (col){
      raNum = Math.floor(quantity*(Math.random()));
      row[col] = partners[raNum];
      console.log("The partner element is: "+partners[raNum]);
      console.log("The array element is: "+row[col]);
      partners.splice(raNum,1);
      quantity--;
    });
  });
*/
  return buildArray;
}
/**
 * Method to generate matrix of the game board
 * @return {void}
 */
function addEventClickInElements(){
  let items = document.getElementsByClassName('item');
  let sizeItems = items.length;
  for (let i = 0; i < sizeItems; i++){
    items[i].addEventListener('click', function (){
      handleGameLogic(this);
    });
  }
}

/**
 * Method to generate matrix of the game board
 * @param {object} item - selected item
 * @return {void}
 */
function handleGameLogic(item){
  currentElement = item;
  countClick++;
  currentElement.classList.toggle('clicked');
  currentElement.innerHTML = currentElement.dataset.value;
  if (countClick == 1) {
    choose1 = currentElement.dataset.value;
    lastId = currentElement.id;
  } else if (countClick == 2){
    lastElement = document.getElementById(lastId);
    if (choose1 == currentElement.dataset.value
      && lastId !== currentElement.id) {
      parent++;
      console.log('SON IGUALES');
      currentElement.style.pointerEvents = "none";
      lastElement.style.pointerEvents = "none";
      if(parent==6){
        alert("HAS COMPLETADO EL JUEGO!!!");
      }
  } else {
    console.log('NO SON IGUALES');
    setTimeout(function () {
      currentElement.classList.toggle('clicked');
      currentElement.innerHTML = '';
      console.log(lastId);

      lastElement.innerHTML = '';
      lastElement.classList.toggle('clicked');
      lastId = '';

    }, 200);
//this.classList.toggle('clicked');
//document.getElementById(lastId).classList.toggle('clicked');
  }
  countClick = 0;
  choose1 = '';
  }
}
/* Init game */
initGame();
